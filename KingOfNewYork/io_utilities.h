#pragma once

#include <string>
#include <vector>

using std::string;
using std::vector;

namespace io_utilities
{
	// types characters to cout like a typewriter (delay is in ms)
	void typewrite(const string& str, int typing_delay = 30);

	bool prompt(const string& msg);

	vector<int> promptChoice(const string& msg, vector<string> strChoices, bool cancelOption = false);

	int promptInt(const string& msg);
}