#pragma once

#include <string>
#include <vector>
#include <memory>

#include "Deck.h"
#include "Card.h"
#include "CardObserver.h"

using std::string;
using std::vector;
using std::shared_ptr;

class CardLoader
{
public:
	const static string DEFAULT_CARD_LIST_URL;

	CardLoader();
	~CardLoader();

	void load(Deck<Card>& deck, vector<shared_ptr<Card>>& freeCards, CardObserver& cardObserver);
};
