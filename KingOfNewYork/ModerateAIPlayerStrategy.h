#pragma once

#include "AIPlayerStrategy.h"

class ModerateAIPlayerStrategy : public AIPlayerStrategy
{
public:
	ModerateAIPlayerStrategy(Player* player);
	~ModerateAIPlayerStrategy();

	void rollDice(std::vector<int>& initialRoll);
	void resolveDice(std::map<Dice::EDiceSymbols, int> diceResults);
	bool flee();
	int destroy(std::vector<Deck<Tile>*> buildingTargets, std::vector<std::shared_ptr<Tile>> militaryTargets);
	void move(std::vector<Borough*> movementOptions, bool mandatoryMovement);
	void buyCards(std::vector<std::shared_ptr<Card>>& shop);
};

