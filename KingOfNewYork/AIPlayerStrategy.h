#pragma once

#include <vector>

#include "PlayerStrategy.h"
#include "Player.h"

class AIPlayerStrategy : public PlayerStrategy
{
public:
	AIPlayerStrategy(Player* player);
	virtual ~AIPlayerStrategy();

	virtual int chooseCharacter(std::vector<std::string> characters);
	virtual int chooseStartingBorough(std::vector<std::string> boroughLabels);
	virtual void rollDice(std::vector<int>& initialRoll);
	virtual void resolveDice(std::map<Dice::EDiceSymbols, int> diceResults);
	virtual bool flee();
	virtual int destroy(std::vector<Deck<Tile>*> buildingTargets, std::vector<std::shared_ptr<Tile>> militaryTargets);
	virtual void move(std::vector<Borough*> movementOptions, bool mandatoryMovement);
	virtual void buyCards(std::vector<std::shared_ptr<Card>>& shop);
};

