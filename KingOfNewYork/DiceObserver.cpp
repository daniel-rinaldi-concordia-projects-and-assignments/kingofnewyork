#include "DiceObserver.h"

#include <iostream>

#include "Game.h"
#include "DiceRoller.h"

using std::cout;

void DiceObserver::onNotify(const Observable* observable)
{
	if (const DiceRoller* roller = dynamic_cast<const DiceRoller*>(observable))
	{
		cout << Game::getInstance().getCurrentPlayer().getName() + "'s roll: " << roller->resultsToString(roller->getPreviousRoll()) << '\n';
	}
}
