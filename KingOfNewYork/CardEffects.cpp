#include "CardEffects.h"

#include <iostream>
#include "io_utilities.h"
#include "Game.h"

CardEffects::CardEffects()
{
}

CardEffects::~CardEffects()
{
}

const std::map<int, bool (*)(Player * owner)> CardEffects::conditions = {
	{ -1, [](Player* owner) { return false; } },
	{
		0, [](Player* owner)
		{
			Player& currentPlayer = Game::getInstance().getCurrentPlayer();

			std::vector<int> results = currentPlayer.getDiceRoller()->getPreviousRoll();
			int ouch_count = 0;
			for (int i : results)
				ouch_count += static_cast<Dice::EDiceSymbols>(i) == Dice::EDiceSymbols::OUCH ? 1 : 0;

			return ouch_count >= 3;
		}
	},
	{
		1, [](Player* owner)
		{
			Player& currentPlayer = Game::getInstance().getCurrentPlayer();

			std::vector<int> results = currentPlayer.getDiceRoller()->getPreviousRoll();
			int victory_count = 0;
			for (int i : results)
				victory_count += static_cast<Dice::EDiceSymbols>(i) == Dice::EDiceSymbols::VICTORY ? 1 : 0;

			return victory_count >= 3;
		}
	},
	{
		28, [](Player* owner)
		{
			Player& currentPlayer = Game::getInstance().getCurrentPlayer();
			return (&currentPlayer == owner && currentPlayer.getPhase() == Player::EPlayerPhase::ROLL_DICE &&
					currentPlayer.getBorough()->getMilitaryUnits().size() >= 3);
		}
	},
	{
		39, [](Player* owner)
		{
			Player& currentPlayer = Game::getInstance().getCurrentPlayer();
			return (&currentPlayer == owner && currentPlayer.getPhase() == Player::EPlayerPhase::STANDBY);
		}
	},
	{
		49, [](Player* owner)
		{
			Player& currentPlayer = Game::getInstance().getCurrentPlayer();
			return (&currentPlayer == owner);
		}
	}
};


const std::map<int, void (*)(Player * owner)> CardEffects::effects = {
	{ -1, [](Player* owner) {} },
	{
		0, [](Player* owner)
		{
			Player& currentPlayer = Game::getInstance().getCurrentPlayer();
			if (owner == nullptr)
			{
				for (auto i = 0; i < Game::getInstance().getShop().size(); i++)
				{
					std::shared_ptr<Card> card = Game::getInstance().getShop()[i];
					if (card->getId() == 0)
					{
						Game::getInstance().getShop().erase(Game::getInstance().getShop().begin() + i);
						currentPlayer.getHand().push_back(card);
						break;
					}
				}
			}
			else
			{
				for (auto i = 0; i < owner->getHand().size(); i++)
				{
					std::shared_ptr<Card> card = owner->getHand()[i];
					if (card->getId() == 0)
					{
						owner->addPoints(EPlayerPointType::VICTORY, -3);
						owner->getHand().erase(owner->getHand().begin() + i);
						currentPlayer.getHand().push_back(card);
						break;
					}
				}
			}

			currentPlayer.addPoints(EPlayerPointType::VICTORY, 3);
		}
	},
	{
		1, [](Player* owner)
		{
			Player& currentPlayer = Game::getInstance().getCurrentPlayer();
			if (owner == nullptr)
			{
				for (auto i = 0; i < Game::getInstance().getShop().size(); i++)
				{
					std::shared_ptr<Card> card = Game::getInstance().getShop()[i];
					if (card->getId() == 1)
					{
						Game::getInstance().getShop().erase(Game::getInstance().getShop().begin() + i);
						currentPlayer.getHand().push_back(card);
						currentPlayer.addPoints(EPlayerPointType::VICTORY, 1);
						break;
					}
				}
			}
			else
			{
				if (&currentPlayer != owner)
				{
					for (auto i = 0; i < owner->getHand().size(); i++)
					{
						std::shared_ptr<Card> card = owner->getHand()[i];
						if (card->getId() == 1)
						{
							owner->getHand().erase(owner->getHand().begin() + i);
							currentPlayer.getHand().push_back(card);

							std::vector<int> results = currentPlayer.getDiceRoller()->getPreviousRoll();
							int victory_count = -2;
							for (int i : results)
								victory_count += static_cast<Dice::EDiceSymbols>(i) == Dice::EDiceSymbols::VICTORY ? 1 : 0;
							victory_count = victory_count > 0 ? victory_count : 0;
							currentPlayer.addPoints(EPlayerPointType::VICTORY, victory_count);
							break;
						}
					}
				}
				else if (&currentPlayer == owner && currentPlayer.getPhase() == Player::EPlayerPhase::ROLL_DICE)
				{
					std::vector<int> results = currentPlayer.getDiceRoller()->getPreviousRoll();
					int victory_count = 0;
					for (int i : results)
						victory_count += static_cast<Dice::EDiceSymbols>(i) == Dice::EDiceSymbols::VICTORY ? 1 : 0;
					currentPlayer.addPoints(EPlayerPointType::VICTORY, victory_count);
				}
			}
		}
	},
	{
		28, [](Player* owner)
		{
			Player& currentPlayer = Game::getInstance().getCurrentPlayer();

			if (&currentPlayer == owner && currentPlayer.getPhase() == Player::EPlayerPhase::ROLL_DICE)
			{
				if (owner->getBorough()->getMilitaryUnits().size() >= 3)
				{
					std::vector<int> roll = owner->getDiceRoller()->getPreviousRoll();
					roll.push_back(static_cast<int>(Dice::EDiceSymbols::ENERGY));
					roll.push_back(static_cast<int>(Dice::EDiceSymbols::DESTROY));
					owner->getDiceRoller()->setPreviousRoll(roll);
				}
			}
		}
	},
	{
		39, [](Player* owner)
		{
			Player& currentPlayer = Game::getInstance().getCurrentPlayer();
			if (&currentPlayer == owner && currentPlayer.getPhase() == Player::EPlayerPhase::STANDBY)
				currentPlayer.addPoints(EPlayerPointType::HEALTH, 1);
		}
	},
	{
		49, [](Player* owner)
		{
			Player& currentPlayer = Game::getInstance().getCurrentPlayer();
			if (&currentPlayer == owner)
			{
				currentPlayer.addPoints(EPlayerPointType::ENERGY, 4);
				currentPlayer.addPoints(EPlayerPointType::HEALTH, -4);
			}
		}
	}
};
