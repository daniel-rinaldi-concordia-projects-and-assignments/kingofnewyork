#pragma once

#include <string>
#include <vector>
#include <map>
#include <algorithm>

using std::string;
using std::vector;

template<typename T>
class Graph
{
private:
	class GraphNode
	{
	public:
		T data;
		vector<GraphNode*>* adjacencyList;
		GraphNode(const T& data);
		~GraphNode();
	};

	std::map<string, GraphNode*> vertices;

public:
	Graph();
	~Graph();

	int size();
	void addVertex(string label, const T& data);
	void removeVertex(string label);
	void addEdge(string labelV1, string labelV2);
	void removeEdge(string labelV1, string labelV2);
	bool isAdjacent(string labelV1, string labelV2) const;
	vector<T*> neighbors(string label) const;
	T& operator [] (string label);
	T operator [] (string label) const;
	vector<string> getLabels();
};

// ****************************************************

template<typename T>
Graph<T>::Graph()
{
}

template<typename T>
Graph<T>::~Graph()
{
	for (auto it = vertices.begin(); it != vertices.end(); it++)
		delete it->second;
	vertices.clear();
}

template<typename T>
int Graph<T>::size()
{
	return vertices.size();
}

template<typename T>
void Graph<T>::addVertex(string label, const T& data) // makes a copy of T by calling T() when passed to GraphNode constructor
{
	vertices.insert(std::map<string, GraphNode*>::value_type(label, new GraphNode(data)));
}

template<typename T>
void Graph<T>::removeVertex(string label)
{
	// TODO:
	// find GraphNode go through its adjList and erase its reference from its neighbors
	// delete GraphNode(it->second) then erase from map
}

// throws out_of_range exception if labels do not exists
template<typename T>
void Graph<T>::addEdge(string labelV1, string labelV2)
{
	GraphNode* v1 = vertices.at(labelV1);
	GraphNode* v2 = vertices.at(labelV2);
	if (std::find(v1->adjacencyList->begin(), v1->adjacencyList->end(), v2) == v1->adjacencyList->end())
	{
		v1->adjacencyList->push_back(v2);
		v2->adjacencyList->push_back(v1);
	}
}

template<typename T>
void Graph<T>::removeEdge(string labelV1, string labelV2)
{
	auto it1 = vertices.find(labelV1);
	auto it2 = vertices.find(labelV2);
	if (it1 != vertices.end() && it2 != vertices.end())
	{
		GraphNode* v1 = it1->second;
		GraphNode* v2 = it2->second;

		for (int repeat = 0; repeat < 2; repeat++)
		{
			for (auto i = 0; i < v1->adjacencyList->size(); i++)
			{
				if (v2 == v1->adjacencyList->at(i))
				{
					v1->adjacencyList->erase(v1->adjacencyList->begin() + i);
					break;
				}
			}

			GraphNode* tmp = v1;
			v1 = v2;
			v2 = tmp;
		}
	}
}

template<typename T>
bool Graph<T>::isAdjacent(string labelV1, string labelV2) const
{
	auto it1 = vertices.find(labelV1);
	auto it2 = vertices.find(labelV2);
	if (it1 != vertices.end() && it2 != vertices.end())
	{
		GraphNode* v1 = it1->second;
		GraphNode* v2 = it2->second;
		GraphNode* vertexToCompare;
		vector<GraphNode*>* adjacentNodes;
		if (v1->adjacencyList->size() >= v2->adjacencyList->size())
		{
			vertexToCompare = v1;
			adjacentNodes = v2->adjacencyList;
		}
		else
		{
			vertexToCompare = v2;
			adjacentNodes = v1->adjacencyList;
		}

		for (auto i = 0; i < adjacentNodes->size(); i++)
		{
			if (vertexToCompare == adjacentNodes->at(i))
				return true;
		}
	}

	return false;
}

// throws out_of_range exception if labels do not exists
template<typename T>
vector<T*> Graph<T>::neighbors(string label) const
{
	vector<T*> neighbors;
	GraphNode* vertex = vertices.at(label);
	for (auto i = 0; i < vertex->adjacencyList->size(); i++)
	{
		GraphNode* node = vertex->adjacencyList->at(i);
		neighbors.push_back(&node->data);
	}

	return neighbors;
}

template<typename T>
T& Graph<T>::operator[](string label)
{
	return vertices[label]->data;
}

template<typename T>
T Graph<T>::operator[](string label) const // makes a copy of T by calling T();
{
	return vertices[label]->data;
}

template<typename T>
vector<string> Graph<T>::getLabels()
{
	vector<string> labels;
	for (auto it = vertices.begin(); it != vertices.end(); it++)
		labels.push_back(it->first);

	return labels;
}

template<typename T>
Graph<T>::GraphNode::GraphNode(const T& data)
{
	adjacencyList = new vector<GraphNode*>();
	this->data = data;
}

template<typename T>
Graph<T>::GraphNode::~GraphNode()
{
	delete adjacencyList;
}