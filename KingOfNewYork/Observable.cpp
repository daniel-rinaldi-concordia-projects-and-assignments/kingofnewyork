#include "Observable.h"

void Observable::registerObserver(Observer* observer)
{
	this->observers.push_back(observer);
}

void Observable::unregisterObserver(Observer* observer)
{
	for (auto i = 0; i < observers.size(); i++)
	{
		observers.remove(observer);
	}
}

void Observable::notify()
{
	for (Observer* observer : observers)
		observer->onNotify(this);
}