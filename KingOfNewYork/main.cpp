
#include <iostream>
#include <ctime>
#include "Application.h"

int main(int argc, char** argv)
{
	// seed rand
	std::srand((unsigned int) std::time(0));

	Application app;
	app.start();

	std::cout << "\nPress Enter to exit the program...";
	std::cin.get();
	return 0;
}