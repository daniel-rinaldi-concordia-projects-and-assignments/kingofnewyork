#include "Player.h"

#include <iostream>
#include <cctype>
#include <algorithm>

#include "io_utilities.h"
#include "Game.h"
#include "PlayerPhaseObserver.h"
#include "HumanPlayerStrategy.h"
#include "AggressiveAIPlayerStrategy.h"
#include "ModerateAIPlayerStrategy.h"

using std::cout;
using std::cin;

Player::Player() : Player("", EStrategy::HUMAN) {}
Player::Player(string name, EStrategy strategy)
{
	this->name = name;
	health_points = 10;
	max_health = 10;
	numDice = 6;
	maxRolls = 3;
	deathFlag = false;
	diceRoller = new DiceRoller();
	phase = EPlayerPhase::NONE;
	sub_phase = EPlayerSubPhase::NONE;

	if (strategy == EStrategy::HUMAN)
		playerStrategy = new HumanPlayerStrategy(this);
	else if (strategy == EStrategy::AI_AGGRESSIVE)
		playerStrategy = new AggressiveAIPlayerStrategy(this);
	else if (strategy == EStrategy::AI_MODERATE)
		playerStrategy = new ModerateAIPlayerStrategy(this);
}

Player::~Player()
{
	delete diceRoller;
	delete playerStrategy;
}

string Player::playerPointTypeToString(EPlayerPointType type)
{
	if (type == EPlayerPointType::ENERGY)
		return "ENERGY";
	else if (type == EPlayerPointType::HEALTH)
		return "HEALTH";
	else if (type == EPlayerPointType::VICTORY)
		return "VICTORY";
	else
		return "";
}

int Player::getPoints(EPlayerPointType player_point_type)
{
	if (player_point_type == EPlayerPointType::HEALTH)
		return health_points;
	else if (player_point_type == EPlayerPointType::ENERGY)
		return energy_points;
	else if (player_point_type == EPlayerPointType::VICTORY)
		return victory_points;
	else
		return 0;
}

void Player::addPoints(EPlayerPointType player_point_type, int n)
{
	if (player_point_type == EPlayerPointType::HEALTH)
	{
		health_points += n;
		health_points = health_points < 0 ? 0 : health_points;
		health_points = health_points > max_health ? max_health : health_points;
	}
	else if (player_point_type == EPlayerPointType::ENERGY)
	{
		energy_points += n;
		energy_points = energy_points < 0 ? 0 : energy_points;
	}
	else if (player_point_type == EPlayerPointType::VICTORY)
	{
		victory_points += n;
		victory_points = victory_points < 0 ? 0 : victory_points;
	}
}

bool Player::isDead()
{
	if (health_points <= 0)
		deathFlag = true;
	return deathFlag;
}

int Player::getMaxHealth()
{
	return max_health;
}

void Player::setMaxHealth(int health)
{
	this->max_health = health;
}

int Player::getZone()
{
	return zone;
}

void Player::setZone(int zone)
{
	this->zone = zone;
}

string Player::getName() const
{
	return name;
}

void Player::setName(string name)
{
	this->name = name;
}

Player::EPlayerPhase Player::getPhase() const
{
	return phase;
}

Player::EPlayerSubPhase Player::getSubPhase() const
{
	return sub_phase;
}

Borough* Player::getBorough() const
{
	return borough;
}

void Player::setBorough(Borough* borough)
{
	this->borough = borough;
}

int Player::getMaxRolls()
{
	return maxRolls;
}

TokenCounter& Player::getTokenCounter()
{
	return tokenCounter;
}

std::vector<std::shared_ptr<Card>>& Player::getHand()
{
	return hand;
}

DiceRoller* Player::getDiceRoller() const
{
	return diceRoller;
}

void Player::update()
{
	if (!isDead()) cout << '\n' << "----- " << name << "'s Turn -----" << '\n';
	standbyPhase();
	resolveDice(rollDice(numDice));
	move();
	buyCards(Game::getInstance().getShop());
	endTurn();

	phase = EPlayerPhase::NONE;
	sub_phase = EPlayerSubPhase::NONE;
}

void Player::render() const
{
	cout << name << '\n';
	cout << "--------------------" << '\n';

	renderPoints();

	if (!titles.empty())
	{
		cout << "TITLES: [ ";
		for (EPlayerTitle title : titles)
		{
			if (title == EPlayerTitle::SUPERSTAR)
				cout << "(SUPERSTAR) ";
			else if (title == EPlayerTitle::DEFENDER_OF_THE_CITY)
				cout << "(DEFENDER OF THE CITY) ";
		}
		cout << "]" << '\n';
	}

	if (!hand.empty())
	{
		cout << "HAND: [" << '\n';
		for (shared_ptr<Card> card : hand)
			card->render();
		cout << "]" << '\n';
	}

	if (tokenCounter.getTokenCount() > 0)
	{
		cout << "TOKENS: ";
		tokenCounter.render();
	}
}

void Player::renderPoints() const
{
	cout << "HEALTH: " << health_points << " | ";
	cout << "ENERGY: " << energy_points << " | ";
	cout << "VICTORY: " << victory_points << '\n';
}

string Player::toString()
{
	string str = "";
	str += "{name:" + name + ", health:" + std::to_string(health_points) + ", energy:" + std::to_string(energy_points) +
		", victory points:" + std::to_string(victory_points) + ", location:" + getBorough()->getName() + "[" + std::to_string(zone) + "]" +
		", hand:[";

	for (std::shared_ptr<Card> card : hand)
		str += card->toString() + ",";
	str += "], tokens:" + tokenCounter.toString();
	str += "}";

	return str;
}

std::map<Dice::EDiceSymbols, int> Player::rollDice(int numDice)
{
	if (maxRolls <= 0 || isDead())
		return std::map<Dice::EDiceSymbols, int>();

	phase = EPlayerPhase::ROLL_DICE;

	sub_phase = EPlayerSubPhase::START;
	this->notify();

	vector<int> results = diceRoller->roll(numDice);

	playerStrategy->rollDice(results);

	std::map<Dice::EDiceSymbols, int> resultSymbols;
	for (int i : diceRoller->getPreviousRoll())
		resultSymbols[static_cast<Dice::EDiceSymbols>(i)]++;

	sub_phase = EPlayerSubPhase::END;
	this->notify();

	return resultSymbols;
}

void Player::resolveDice(std::map<Dice::EDiceSymbols, int> diceResults)
{
	if (isDead() || diceResults.empty()) return;

	phase = EPlayerPhase::RESOLVE_DICE;

	sub_phase = EPlayerSubPhase::START;
	this->notify();

	playerStrategy->resolveDice(diceResults);

	sub_phase = EPlayerSubPhase::END;
	this->notify();
}

int Player::chooseCharacter(vector<string> characters)
{
	return playerStrategy->chooseCharacter(characters);
}

int Player::chooseStartingBorough(vector<string> boroughLabels)
{
	return playerStrategy->chooseStartingBorough(boroughLabels);
}

void Player::standbyPhase()
{
	if (isDead()) return;

	phase = EPlayerPhase::STANDBY;

	diceRoller->clear();

	sub_phase = EPlayerSubPhase::START;
	this->notify();

	if (borough == Game::getInstance().getGameBoard().getMainBorough())
	{
		if (zone == 0)
		{
			addPoints(EPlayerPointType::ENERGY, 1);
			addPoints(EPlayerPointType::VICTORY, 1);
			io_utilities::typewrite(name + " has been awarded 1 ENERGY and 1 VICTORY points\n");
		}
		else if (zone == 1)
		{
			addPoints(EPlayerPointType::ENERGY, 1);
			addPoints(EPlayerPointType::VICTORY, 2);
			io_utilities::typewrite(name + " has been awarded 1 ENERGY and 2 VICTORY points\n");
		}
		else if (zone == 2)
		{
			addPoints(EPlayerPointType::ENERGY, 2);
			addPoints(EPlayerPointType::VICTORY, 2);
			io_utilities::typewrite(name + " has been awarded 2 ENERGY and 2 VICTORY points\n");
		}
	}

	sub_phase = EPlayerSubPhase::END;
	this->notify();
}

void Player::resolve(Dice::EDiceSymbols diceSymbol, int n)
{
	if (diceSymbol == Dice::EDiceSymbols::ENERGY)
	{
		addPoints(EPlayerPointType::ENERGY, n);
		io_utilities::typewrite(name + ", has gained " + std::to_string(n) + " ENERGY\n");
	}
	else if (diceSymbol == Dice::EDiceSymbols::HEART)
		resolveHeart(n);
	else if (diceSymbol == Dice::EDiceSymbols::VICTORY)
		resolveVictory(n);
	else if (diceSymbol == Dice::EDiceSymbols::ATTACK)
		attack(n);
	else if (diceSymbol == Dice::EDiceSymbols::DESTROY)
		resolveDestroy(n);
	else if (diceSymbol == Dice::EDiceSymbols::OUCH)
		resolveOuch(n);
}

void Player::resolveHeart(int health)
{
	if (this->borough != Game::getInstance().getGameBoard().getMainBorough())
	{
		addPoints(EPlayerPointType::HEALTH, health);
		io_utilities::typewrite(name + ", has healed for " + std::to_string(health) + " HEART\n");
	}
	else
		io_utilities::typewrite(name + ", No time to heal when you're in " + borough->getName() + "\n");
}

void Player::resolveVictory(int victory)
{
	// nothing to do
}

void Player::move()
{
	if (isDead()) return;

	phase = EPlayerPhase::MOVE;

	sub_phase = EPlayerSubPhase::START;
	this->notify();

	vector<Borough*> movementOptions;
	Borough* mainBorough = Game::getInstance().getGameBoard().getMainBorough();
	if (borough == mainBorough)
	{ 
		// in main borough
		if (borough->getPlayers().size() == 1)
		{ 
			// you are the only player in the main borough
			resolveMovement(movementOptions);
		}
		else
		{ 
			// there is someone else in the main borough with you
			if (Game::getInstance().getGameRulesPolicy() == Game::EGameRulesPolicy::SixPlayerPolicy)
			{
				resolveMovement(movementOptions);
			}
			else if (Game::getInstance().getGameRulesPolicy() == Game::EGameRulesPolicy::FourPlayerPolicy)
			{
				movementOptions.clear();
				for (auto i = 1; i < Game::getInstance().getGameBoard().getBoroughs().size(); i++)
					movementOptions.push_back(&Game::getInstance().getGameBoard().getBoroughs()[i]);
				
				for (int i = 0; i < movementOptions.size(); i++)
				{
					if (movementOptions[i]->isFull())
						movementOptions[i] = nullptr; // mark for removal
				}
				movementOptions.erase(remove(movementOptions.begin(), movementOptions.end(), nullptr), movementOptions.end()); // remove marked
				resolveMovement(movementOptions, true);
			}
		}
	}
	else
	{ 
		// not in main borough
		if (mainBorough->isFull())
		{ 
			// main borough is full
			movementOptions.clear();
			for (auto i = 1; i < Game::getInstance().getGameBoard().getBoroughs().size(); i++)
				movementOptions.push_back(&Game::getInstance().getGameBoard().getBoroughs()[i]);

			for (int i = 0; i < movementOptions.size(); i++)
			{
				if (movementOptions[i]->isFull() || movementOptions[i] == borough)
					movementOptions[i] = nullptr; // mark for removal
			}
			movementOptions.erase(remove(movementOptions.begin(), movementOptions.end(), nullptr), movementOptions.end()); // remove marked
			resolveMovement(movementOptions);
		}
		else
		{ 
			// main borough has an available slot
			moveToBorough(mainBorough);
		}

	}

	sub_phase = EPlayerSubPhase::END;
	this->notify();
}

void Player::resolveMovement(std::vector<Borough*> movementOptions, bool mandatoryMovement)
{
	if (movementOptions.empty())
	{
		if (this->borough == Game::getInstance().getGameBoard().getMainBorough())
		{
			if (zone < 2)
				zone++;
			io_utilities::typewrite(name + " has moved up to " + (zone == 1 ? "middle " : "upper ") + borough->getName() + "\n");
		}
	}
	else
		playerStrategy->move(movementOptions, mandatoryMovement);
}

void Player::moveToBorough(Borough* borough)
{
	zone = 0;
	this->borough->removePlayer(this);
	this->borough = borough;
	this->borough->addPlayer(this);

	io_utilities::typewrite(name + " has moved to " + this->borough->getName() + "\n");

	if (this->borough == Game::getInstance().getGameBoard().getMainBorough())
		addPoints(EPlayerPointType::VICTORY, 1);
}

void Player::buyCards(std::vector<std::shared_ptr<Card>>& shop)
{
	if (isDead()) return;

	phase = EPlayerPhase::BUY_CARDS;

	sub_phase = EPlayerSubPhase::START;
	this->notify();

	playerStrategy->buyCards(shop);

	sub_phase = EPlayerSubPhase::END;
	this->notify();
}

void Player::resolveDestroy(int destruction)
{
	vector<Deck<Tile>>& tileDecks = borough->getTileDecks();
	vector<shared_ptr<Tile>>& militaryUnits = borough->getMilitaryUnits();
	vector<Deck<Tile>*> buildingTargets;
	vector<shared_ptr<Tile>> militaryTargets;

	for (auto i = 0; i < tileDecks.size(); i++)
	{
		if (tileDecks[i].size() > 0 && tileDecks[i].top()->getDurability() <= destruction)
			buildingTargets.push_back(&tileDecks[i]);
	}
	for (auto i = 0; i < militaryUnits.size(); i++)
	{
		if (militaryUnits.size() > 0 && militaryUnits[i]->getDurability() <= destruction)
			militaryTargets.push_back(militaryUnits[i]);
	}

	if (!buildingTargets.empty() || !militaryTargets.empty())
	{
		int selection = -1;
		while (true)
		{
			io_utilities::typewrite(name + ", select a target to destroy:\n");
			for (auto i = 0; i < (buildingTargets.size() + militaryTargets.size()); i++)
			{
				if (i < buildingTargets.size())
				{
					cout << i << ": ";
					buildingTargets[i]->top()->render();
					cout << '\n';
				}
				else if (i < militaryTargets.size())
				{
					cout << i << ": ";
					militaryTargets[i]->render();
					cout << '\n';
				}
			}

			selection = playerStrategy->destroy(buildingTargets, militaryTargets);
			if (selection >= 0)
				break;
		}

		if (selection < buildingTargets.size())
		{
			shared_ptr<Tile> ptrTile = buildingTargets[selection]->draw();
			addPoints(ptrTile->getPointType(), ptrTile->getPoints());
			destruction -= ptrTile->getDurability();
			cout << Tile::tileFaceToString(ptrTile->getFace()) << " building destroyed!" << '\n';
			ptrTile->flip();
			borough->getMilitaryUnits().push_back(ptrTile);
		}
		else if (selection < militaryTargets.size())
		{
			selection -= buildingTargets.size();
			shared_ptr<Tile> ptrTile = militaryTargets[selection];
			addPoints(ptrTile->getPointType(), ptrTile->getPoints());
			destruction -= ptrTile->getDurability();
			militaryUnits.erase(std::remove(militaryUnits.begin(), militaryUnits.end(), militaryTargets[selection]), militaryUnits.end());
			cout << Tile::tileFaceToString(ptrTile->getFace()) << " unit destroyed!" << '\n';
		}

		this->resolveDestroy(destruction);
	}
	else
		io_utilities::typewrite(name + ", there are no available targets to destroy.\n");
}

void Player::resolveOuch(int ouch)
{
	bool militaryResponded = false;

	if (ouch == 1)
	{
		int damage = getBorough()->getMilitaryUnits().size();
		addPoints(EPlayerPointType::HEALTH, -damage);
		if (damage > 0)
		{
			militaryResponded = true;
			io_utilities::typewrite(name + " has suffered " + std::to_string(damage) + " damage from military forces. \n");
		}

	}
	else if (ouch == 2)
	{
		for (Player* p : borough->getPlayers())
		{
			int damage = getBorough()->getMilitaryUnits().size();
			p->addPoints(EPlayerPointType::HEALTH, -damage);
			if (damage > 0)
			{
				militaryResponded = true;
				io_utilities::typewrite(p->name + " has suffered " + std::to_string(damage) + " damage from military forces. \n");
			}
		}
	}
	else if (ouch > 2)
	{
		for (Player* p : Game::getInstance().getPlayers())
		{
			int damage = p->getBorough()->getMilitaryUnits().size();
			p->addPoints(EPlayerPointType::HEALTH, -damage);
			if (damage > 0)
			{
				militaryResponded = true;
				io_utilities::typewrite(p->name + " has suffered " + std::to_string(damage) + " damage from military forces. \n");
			}
		}
	}

	if (!militaryResponded)
		io_utilities::typewrite("There is no response from the military forces. \n");
}

void Player::attack(int atk)
{
	bool hasAttacked = false;

	if (borough == Game::getInstance().getGameBoard().getMainBorough())
	{ 
		// in main borough
		std::string mainBoroughName = Game::getInstance().getGameBoard().getMainBorough()->getName();

		std::vector<Borough*> neighbors;
		for (auto i = 1; i < Game::getInstance().getGameBoard().getBoroughs().size(); i++)
			neighbors.push_back(&Game::getInstance().getGameBoard().getBoroughs()[i]);

		for (Borough* borough : neighbors)
		{
			for (Player* player : borough->getPlayers())
			{
				if (player != nullptr)
				{
					hasAttacked = true;
					player->addPoints(EPlayerPointType::HEALTH, -(atk));
					io_utilities::typewrite(player->name + " has suffered " + std::to_string(atk) + " damage from " + this->name + ".\n");
				}
			}
		}
	}
	else
	{ 
		// not in main borough
		for (Player* player : Game::getInstance().getGameBoard().getMainBorough()->getPlayers())
		{
			if (player != nullptr)
			{
				hasAttacked = true;
				player->addPoints(EPlayerPointType::HEALTH, -(atk));
				io_utilities::typewrite(player->name + " has suffered " + std::to_string(atk) + " damage from " + this->name + ".\n");

				if (player->isDead())
				{
					io_utilities::typewrite(player->getName() + " is dead!");
					Game::getInstance().getGameBoard().getMainBorough()->removePlayer(player);
				}
				else
				{
					if (player->playerStrategy->flee())
					{
						std::string mainBoroughName = Game::getInstance().getGameBoard().getMainBorough()->getName();

						std::vector<Borough*> movementOptions;
						for (auto i = 1; i < Game::getInstance().getGameBoard().getBoroughs().size(); i++)
							movementOptions.push_back(&Game::getInstance().getGameBoard().getBoroughs()[i]);

						for (int i = 0; i < movementOptions.size(); i++)
						{
							if (movementOptions[i]->isFull())
								movementOptions[i] = nullptr; // mark for removal
						}
						movementOptions.erase(remove(movementOptions.begin(), movementOptions.end(), nullptr), movementOptions.end()); // remove marked;

						player->resolveMovement(movementOptions, true);
					}
				}
			}
		}
	}

	if (!hasAttacked)
		io_utilities::typewrite(this->name + ", there are no targets to attack.\n");
}

void Player::endTurn()
{
	if (isDead()) return;

	phase = EPlayerPhase::END;

	sub_phase = EPlayerSubPhase::START;
	this->notify();

	sub_phase = EPlayerSubPhase::END;
	this->notify();
}

void Player::testCardEffects()
{
	for (std::shared_ptr<Card> card : Game::getInstance().getGoalCards())
	{
		card->activateEffect(nullptr);
	}
	for (std::shared_ptr<Card> card : this->hand)
	{
		card->activateEffect(this);
	}
	for (Player* player : Game::getInstance().getPlayers())
	{
		for (std::shared_ptr<Card> card : player->getHand())
		{
			card->activateEffect(player);
		}
	}
}