#pragma once

#include <string>

#include "Observable.h"
#include "GameObject.h"
#include "Player.h"

using std::string;

class Player;

class Card : public GameObject, public Observable
{
public:
	enum class CARD_TYPE { GOAL, KEEP, DISCARD };

private:
	int id;
	string name;
	int cost;
	CARD_TYPE type;
	string desc;

public:
	Card(int id);
	Card(string name, int cost, CARD_TYPE type, string desc);
	~Card();

	static string cardTypeToString(CARD_TYPE type);

	int getId();
	std::string getName();
	void setName(string name);
	int getCost();
	void setCost(int cost);
	CARD_TYPE getType();
	void setType(CARD_TYPE type);
	std::string getDesc();
	void setDesc(string desc);

	void activateEffect(Player* owner);

	void render() const override;

	string toString();
};

