#include "CardLoader.h"

#include <fstream>
#include "common_utilities.h"

const std::string CardLoader::DEFAULT_CARD_LIST_URL = "data/cards/cards.dat";

CardLoader::CardLoader()
{
}

CardLoader::~CardLoader()
{
}

void CardLoader::load(Deck<Card>& deck, vector<shared_ptr<Card>>& goalCards, CardObserver& cardObserver)
{
	std::ifstream inFile(DEFAULT_CARD_LIST_URL);

	if (!inFile)
		throw string("Cannot open or read file: '" + DEFAULT_CARD_LIST_URL + "'");

	string line;
	while (getline(inFile, line))
	{
		common_utilities::strTrim(line);
		if (line == "") continue;

		vector<string> pair(2);
		bool placeInDeck = true;

		// id
		pair.clear();
		common_utilities::strSplit(line, pair, ':');
		common_utilities::strTrim(pair[1]);
		int id = common_utilities::isInt(pair[1]) ? std::stoi(pair[1]) : -1;
		shared_ptr<Card> ptrCard(new Card(id));

		ptrCard->registerObserver(&cardObserver);

		// skip imageURL
		pair.clear();
		getline(inFile, line);

		// name
		pair.clear();
		getline(inFile, line);
		common_utilities::strTrim(line);
		common_utilities::strSplit(line, pair, ':');
		ptrCard->setName(pair[1]);

		// type
		pair.clear();
		getline(inFile, line);
		common_utilities::strTrim(line);
		common_utilities::strSplit(line, pair, ':');
		std::transform(pair[1].begin(), pair[1].end(), pair[1].begin(), ::toupper);
		if (pair[1] == Card::cardTypeToString(Card::CARD_TYPE::GOAL))
		{
			ptrCard->setType(Card::CARD_TYPE::GOAL);
			placeInDeck = false;
		}
		else if (pair[1] == Card::cardTypeToString(Card::CARD_TYPE::DISCARD))
			ptrCard->setType(Card::CARD_TYPE::DISCARD);
		else if (pair[1] == Card::cardTypeToString(Card::CARD_TYPE::KEEP))
			ptrCard->setType(Card::CARD_TYPE::KEEP);

		// cost
		pair.clear();
		getline(inFile, line);
		common_utilities::strTrim(line);
		common_utilities::strSplit(line, pair, ':');
		common_utilities::strTrim(pair[1]);
		ptrCard->setCost(common_utilities::isInt(pair[1]) ? std::stoi(pair[1]) : 0);

		// desc
		pair.clear();
		getline(inFile, line);
		common_utilities::strTrim(line);
		common_utilities::strSplit(line, pair, ':');
		common_utilities::strTrim(pair[1]);
		ptrCard->setDesc(pair[1]);

		if (placeInDeck)
			deck.add(ptrCard);
		else
			goalCards.push_back(ptrCard);
	}

	inFile.close();
}