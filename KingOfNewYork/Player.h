#pragma once

#include <string>
#include <map>
#include <vector>
#include <memory>

#include "Observable.h"
#include "GameObject.h"
#include "Card.h"
#include "Borough.h"
#include "TokenCounter.h"
#include "DiceRoller.h"
#include "PlayerStrategy.h"
#include "EPlayerPointType.h"

using std::shared_ptr;
using std::vector;
using std::string;

class PlayerStrategy;
class Borough;
class Card;

class Player : public GameObject, public Observable
{
public:
	enum class EPlayerTitle { NONE, SUPERSTAR, DEFENDER_OF_THE_CITY };
	enum class EPlayerPhase { NONE, STANDBY, ROLL_DICE, RESOLVE_DICE, MOVE, BUY_CARDS, END };
	enum class EPlayerSubPhase { NONE, START, END };
	enum class EStrategy { HUMAN, AI_AGGRESSIVE, AI_MODERATE };

private:
	int health_points;
	int max_health;
	int energy_points;
	int victory_points;
	bool deathFlag;
	std::string name;
	std::vector<EPlayerTitle> titles;
	EPlayerPhase phase;
	EPlayerSubPhase sub_phase;
	int zone;
	Borough* borough;
	std::vector<shared_ptr<Card>> hand;
	TokenCounter tokenCounter;
	DiceRoller* diceRoller;
	int numDice;
	int maxRolls;
	PlayerStrategy* playerStrategy;

public:
	Player();
	Player(string name, EStrategy strategy = EStrategy::HUMAN);
	~Player();

	static std::string playerPointTypeToString(EPlayerPointType type);
	int getPoints(EPlayerPointType player_point_type);
	void addPoints(EPlayerPointType player_point_type, int n);
	bool isDead();
	int getMaxHealth();
	void setMaxHealth(int health);
	int getZone();
	void setZone(int zone);
	std::string getName() const;
	void setName(std::string name);
	EPlayerPhase getPhase() const;
	EPlayerSubPhase getSubPhase() const;
	Borough* getBorough() const;
	void setBorough(Borough* borough);
	int getMaxRolls();
	TokenCounter& getTokenCounter();
	vector<shared_ptr<Card>>& getHand();
	DiceRoller* getDiceRoller() const;

	void update();
	void render() const;
	void renderPoints() const;

	std::string toString();

	int chooseCharacter(vector<string> characters);
	int chooseStartingBorough(vector<string> boroughLabels);
	void standbyPhase();
	std::map<Dice::EDiceSymbols, int> rollDice(int numDice);

	void resolveDice(std::map<Dice::EDiceSymbols, int> diceResults);
	void resolve(Dice::EDiceSymbols diceSymbol, int n);
	void resolveHeart(int health);
	void resolveVictory(int victory);
	void resolveDestroy(int destruction);
	void resolveOuch(int ouch);
	void attack(int atk);

	void move();
	void resolveMovement(vector<Borough*> movementOptions, bool mandatoryMovement = false);
	void moveToBorough(Borough* borough);

	void buyCards(vector<shared_ptr<Card>>& shop);
	void endTurn();

	void testCardEffects();
};

