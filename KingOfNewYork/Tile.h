#pragma once

#include <string>

#include "GameObject.h"
#include "Player.h"
#include "EPlayerPointType.h"

using std::string;

class Tile : public GameObject
{
public:
	enum class ETileType { BUILDING, MILITARY };
	enum class ETileFace { OFFICE, HOSPITAL, POWER_PLANT, INFANTRY, JET, TANK };

private:
	int points;
	int durability;
	EPlayerPointType player_point_type;
	ETileType type;
	ETileFace face;
public:
	Tile();
	Tile(ETileType type, ETileFace face, int durability);
	~Tile();

	static string tileTypeToString(ETileType type);
	static string tileFaceToString(ETileFace face);

	ETileType getType();
	ETileFace getFace();
	EPlayerPointType getPointType();
	int getPoints();
	int getDurability();

	void flip();

	void render() const override;

	string toString();
};

