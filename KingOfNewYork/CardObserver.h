#pragma once

#include "Observer.h"

class CardObserver : public Observer
{
public:
	void onNotify(const Observable* observable);
};

