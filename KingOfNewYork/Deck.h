#pragma once

#include <string>
#include <vector>
#include <algorithm>
#include <memory>

#include "GameObject.h"

using std::string;
using std::vector;
using std::shared_ptr;

template<typename T>
class Deck : public GameObject
{
private:
	vector<shared_ptr<T>> items;

public:
	Deck();
	~Deck();

	shared_ptr<T> top();
	shared_ptr<T> draw();
	void shuffle();
	void add(shared_ptr<T> item, bool shuffle = false);
	int size() const;

	void render() const override;
};

// *********************************************************************

template<typename T>
Deck<T>::Deck()
{
}

template<typename T>
Deck<T>::~Deck()
{
}

template<typename T>
shared_ptr<T> Deck<T>::top()
{
	return items.size() > 0 ? items[0] : nullptr;
}

template<typename T>
shared_ptr<T> Deck<T>::draw()
{
	shared_ptr<T> item = nullptr;
	if (!items.empty())
	{
		item = items[0];
		items.erase(items.begin());
	}
	return item;
}

template<typename T>
void Deck<T>::shuffle()
{
	std::random_shuffle(items.begin(), items.end());
}

template<typename T>
void Deck<T>::add(shared_ptr<T> item, bool shuffle)
{
	if (item.get() != nullptr)
	{
		items.push_back(item);
		if (shuffle)
			this->shuffle();
	}
}

template<typename T>
int Deck<T>::size() const
{
	return items.size();
}

template<typename T>
void Deck<T>::render() const
{
	//
}
