#include "AIPlayerStrategy.h"

#include <iostream>

#include "io_utilities.h"
#include "Game.h"

AIPlayerStrategy::AIPlayerStrategy(Player* player) : PlayerStrategy(player)
{
}

AIPlayerStrategy::~AIPlayerStrategy()
{
}

int AIPlayerStrategy::chooseCharacter(std::vector<std::string> characters)
{
	io_utilities::typewrite(player->getName() + " please select a character : \n");
	int chosenIndex = std::rand() % characters.size();

	std::cout << player->getName() << " => ";
	player->setName(characters[chosenIndex]);
	std::cout << player->getName() << std::endl;

	return chosenIndex;
}

int AIPlayerStrategy::chooseStartingBorough(std::vector<std::string> boroughLabels)
{
	io_utilities::typewrite(player->getName() + " please select a starting borough : \n");
	int chosenIndex = std::rand() % boroughLabels.size();

	std::cout << player->getName() << " => ";
	Game::getInstance().getGameBoard().getBoroughs()[chosenIndex + 1].addPlayer(player);
	player->setBorough(&Game::getInstance().getGameBoard().getBoroughs()[chosenIndex + 1]);
	std::cout << player->getBorough()->getName() << std::endl;

	return chosenIndex;
}

void AIPlayerStrategy::rollDice(std::vector<int>& results)
{
	// does not reroll
}

void AIPlayerStrategy::resolveDice(std::map<Dice::EDiceSymbols, int> diceResults)
{
	for (auto it = diceResults.begin(); it != diceResults.end(); it++)
		player->resolve(it->first, it->second);
}

bool AIPlayerStrategy::flee()
{
	return (player->getPoints(EPlayerPointType::HEALTH) <= player->getMaxHealth() / 2);
}

int AIPlayerStrategy::destroy(std::vector<Deck<Tile>*> buildingTargets, std::vector<std::shared_ptr<Tile>> militaryTargets)
{
	int size = buildingTargets.size() + militaryTargets.size();
	return std::rand() % size;
}

void AIPlayerStrategy::move(std::vector<Borough*> movementOptions, bool mandatoryMovement)
{
	io_utilities::typewrite(player->getName() + " where would you like to move?");
	int size = movementOptions.size() + (mandatoryMovement ? 0 : 1);
	int selection = std::rand() % size;

	if (selection < movementOptions.size())
		player->moveToBorough(movementOptions[selection]);
}

void AIPlayerStrategy::buyCards(std::vector<std::shared_ptr<Card>>& shop)
{
	bool loop = true;
	while (loop)
	{
		if (shop.empty())
		{
			io_utilities::typewrite("There is nothing left in the shop to buy.\n");
			break;
		}

		int energy = player->getPoints(EPlayerPointType::ENERGY);
		io_utilities::typewrite(player->getName() + ", you have " + std::to_string(energy) + " ENERGY, select a card from the shop to buy.\n");

		std::vector<int> choices;
		for (auto i = 0; i < shop.size(); i++)
		{
			if (shop[i]->getCost() <= energy)
				choices.push_back(i);
		}
		if (energy >= 2) choices.push_back(-2);
		choices.push_back(-1);

		int selection = std::rand() % choices.size();

		if (choices[selection] == -1) break;
		else if (choices[selection] == -2)
		{
			player->addPoints(EPlayerPointType::ENERGY, -2);

			for (auto s = 0; s < shop.size(); s++)
				Game::getInstance().getDiscardPile().push_back(shop[s]);
			shop.clear();

			for (auto s = 0; s < 3; s++)
			{
				std::shared_ptr<Card> drawnCard = Game::getInstance().getCardDeck().draw();
				if (drawnCard != nullptr)
					shop.push_back(drawnCard);
				else
					break;
			}

			io_utilities::typewrite(player->getName() + " has chosen to refresh the shop. \n");

			if (!shop.empty())
			{
				Game::getInstance().renderShop();
			}
		}
		else
		{
			if (!(shop[choices[selection]]->getType() == Card::CARD_TYPE::DISCARD))
				player->getHand().push_back(shop[choices[selection]]);
			else
			{
				player->getHand().push_back(shop[choices[selection]]);
				Game::getInstance().getDiscardPile().push_back(player->getHand()[player->getHand().size() - 1]);
				player->getHand().pop_back();
			}

			shop.erase(shop.begin() + choices[selection]);

			std::shared_ptr<Card> drawnCard = Game::getInstance().getCardDeck().draw();
			if (drawnCard != nullptr)
				shop.push_back(drawnCard);

			Game::getInstance().renderShop();
		}
	}
}
