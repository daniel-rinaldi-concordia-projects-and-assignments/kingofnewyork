#pragma once

#include <string>
#include <vector>

using std::string;
using std::vector;

namespace common_utilities
{
	string getDetailedCurrentTimeStamp();
	string& strTrim(string& str);
	void strSplit(std::string str, std::vector<std::string>& container, char delim);
	bool isInt(char c);
	bool isInt(string str);
	vector<string> dirList(const string& path, const string& extension);
}