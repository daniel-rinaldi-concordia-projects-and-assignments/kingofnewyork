#pragma once

#include "Observer.h"

class DiceObserver : public Observer
{
public:
	void onNotify(const Observable* observable);
};

