#include "Application.h"

Application::Application()
{
	Game::getInstance();
}

Application::~Application()
{
	//
}

void Application::start()
{
	Game::getInstance().setup();
	Game::getInstance().loop();

	exit();
}

void Application::exit()
{
	//
}
