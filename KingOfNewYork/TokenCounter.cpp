#include "TokenCounter.h"

#include <iostream>

using std::cout;

TokenCounter::TokenCounter() : counters(static_cast<int>(ETokenType::SIZE))
{
}

TokenCounter::~TokenCounter()
{
}

int TokenCounter::getTokenCount() const
{
	int sum = 0;
	for (int count : counters)
		sum += count;
	return sum;
}

int TokenCounter::getTokenCount(ETokenType token_type) const
{
	int token_id = static_cast<int>(token_type);
	return counters[token_id];
}

void TokenCounter::removeToken(ETokenType token_type)
{
	int token_id = static_cast<int>(token_type);
	if (counters[token_id] > 0)
		counters[token_id]--;
}

void TokenCounter::addToken(ETokenType token_type)
{
	int token_id = static_cast<int>(token_type);
	counters[token_id]++;
}

void TokenCounter::render() const
{
	cout << "{ ";
	for (auto i = 0; i < static_cast<int>(ETokenType::SIZE); i++)
	{
		ETokenType type = static_cast<ETokenType>(i);
		if (counters[i] > 0)
		{
			if (type == ETokenType::CARAPACE)
				cout << "(CARAPACE: " + std::to_string(counters[i]) + ") ";
			else if (type == ETokenType::SOUVENIR)
				cout << "(SOUVENIR: " + std::to_string(counters[i]) + ") ";
			else if (type == ETokenType::WEB)
				cout << "(WEB: " + std::to_string(counters[i]) + ") ";
			else if (type == ETokenType::JINX)
				cout << "(JINX: " + std::to_string(counters[i]) + ") ";
		}
	}
	cout << "}" << '\n';
}

string TokenCounter::toString()
{
	string str = "{ ";
	for (auto i = 0; i < static_cast<int>(ETokenType::SIZE); i++)
	{
		ETokenType type = static_cast<ETokenType>(i);
		if (type == ETokenType::CARAPACE)
			str += "(CARAPACE: " + std::to_string(counters[i]) + ") ";
		else if (type == ETokenType::SOUVENIR)
			str += "(SOUVENIR: " + std::to_string(counters[i]) + ") ";
		else if (type == ETokenType::WEB)
			str += "(WEB: " + std::to_string(counters[i]) + ") ";
		else if (type == ETokenType::JINX)
			str += "(JINX: " + std::to_string(counters[i]) + ") ";
	}
	str += "}";
	return str;
}


