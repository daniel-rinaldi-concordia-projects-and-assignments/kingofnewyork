#include "common_utilities.h"

#include <ctime>
#include <regex>
#include <sstream>
#include <iostream>

#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING
#include <experimental/filesystem>
namespace filesystem = std::experimental::filesystem;

namespace common_utilities
{
	string getDetailedCurrentTimeStamp()
	{
		std::time_t time = std::time(0);
		std::tm now;
		localtime_s(&now, &time);

		string year = std::to_string(now.tm_year + 1900);
		string month = now.tm_mon < 9 ? "0" + std::to_string(now.tm_mon + 1) : std::to_string(now.tm_mon + 1);
		string day = now.tm_mday < 10 ? "0" + std::to_string(now.tm_mday) : std::to_string(now.tm_mday);
		string hour = now.tm_hour < 10 ? "0" + std::to_string(now.tm_hour) : std::to_string(now.tm_hour);
		string min = now.tm_min < 10 ? "0" + std::to_string(now.tm_min) : std::to_string(now.tm_min);
		string sec = now.tm_sec < 10 ? "0" + std::to_string(now.tm_sec) : std::to_string(now.tm_sec);

		return (year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec);
	}

	string& strTrim(string& str)
	{
		return str = std::regex_replace(str, std::regex("^\\s+|\\s+$"), "");
	}

	void strSplit(std::string str, std::vector<std::string>& container, char delim)
	{
		std::stringstream ss(str);
		string token;
		while (getline(ss, token, delim)) {
			container.push_back(token);
		}
	}

	bool isInt(char c)
	{
		return (c > 47 && c < 58);
	}

	bool isInt(string str)
	{
		for (char c : str)
		{
			if (!isInt(c)) return false;
		}

		return true;
	}

	vector<string> dirList(const string& path, const string& extension)
	{
		vector<string> vec;
		for (auto& p : filesystem::directory_iterator(path))
		{
			if (p.path().has_extension() && p.path().extension() == extension)
			{
				std::string pathStr = std::regex_replace(p.path().string(), std::regex("\\\\"), "/");
				std::vector<std::string> strTokens;
				strSplit(pathStr, strTokens, '/');
				std::string fileName = strTokens[strTokens.size() - 1];
				vec.push_back(fileName);
			}
		}

		return vec;
	}
}