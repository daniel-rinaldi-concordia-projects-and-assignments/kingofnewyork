#pragma once

#include <vector>

#include "PlayerStrategy.h"
#include "Player.h"

class HumanPlayerStrategy : public PlayerStrategy
{
public:
	HumanPlayerStrategy(Player* player);
	~HumanPlayerStrategy();

	int chooseCharacter(std::vector<std::string> characters);
	int chooseStartingBorough(std::vector<std::string> boroughLabels);
	void rollDice(std::vector<int>& initialRoll);
	void resolveDice(std::map<Dice::EDiceSymbols, int> diceResults);
	bool flee();
	int destroy(std::vector<Deck<Tile>*> buildingTargets, std::vector<std::shared_ptr<Tile>> militaryTargets);
	void move(std::vector<Borough*> movementOptions, bool mandatoryMovement);
	void buyCards(std::vector<std::shared_ptr<Card>>& shop);
};

