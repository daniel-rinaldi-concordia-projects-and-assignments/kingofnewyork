#include "Borough.h"

#include <iostream>

#include "Game.h";

using std::cout;

Borough::Borough() :Borough("") {}
Borough::Borough(string name)
{
	this->name = name;
	player_limit = 2;
}

Borough::~Borough()
{
}

void Borough::setName(string name)
{
	this->name = name;
}

string Borough::getName()
{
	return name;
}

void Borough::setPlayerLimit(int limit)
{
	if (limit > 0 && limit < 3)
		player_limit = limit;
}

int Borough::getPlayerLimit()
{
	return player_limit;
}

std::vector<Player*>& Borough::getPlayers()
{
	return players;
}

bool Borough::addPlayer(Player* player)
{
	if (players.size() < player_limit)
	{
		players.push_back(player);
		return true;
	}
	else
		return false;
}

void Borough::removePlayer(Player* player)
{
	for (auto i = 0; i < players.size(); i++)
	{
		if (players[i] == player)
			players.erase(players.begin() + i);
	}
}

bool Borough::isFull()
{
	return !(players.size() < player_limit);
}

std::vector<Deck<Tile>>& Borough::getTileDecks()
{
	return tileDecks;
}

void Borough::setTileDecks(vector<Deck<Tile>> decks)
{
	tileDecks = decks;
}

vector<shared_ptr<Tile>>& Borough::getMilitaryUnits()
{
	return militaryUnits;
}

TokenCounter& Borough::getTokenCounter()
{
	return tokenCounter;
}

void Borough::render() const
{
	cout << name << "" << '\n';
	cout << "--------------------" << '\n';

	if (!players.empty())
	{
		cout << "MONSTERS: ";
		for (Player* player : this->players)
		{
			string zonestr = "";
			if (this == Game::getInstance().getGameBoard().getMainBorough())
			{
				zonestr = " | ";
				int z = player->getZone() + 1;
				if (z <= 1) zonestr += "lower";
				else if (z == 2) zonestr += "middle";
				else zonestr += "upper";
			}
			cout << "(" << player->getName() << zonestr << ") ";
		}
		cout << '\n';
	}


	if (!tileDecks.empty() && (tileDecks[0].size() > 0 || tileDecks[1].size() > 0 || tileDecks[2].size() > 0))
	{
		cout << "BUILDINGS: [ ";
		for (Deck<Tile> deck : tileDecks)
		{
			if (deck.size() > 0)
			{
				deck.top()->render();
				cout << " ";
			}
		}
		cout << "] " << '\n';
	}

	if (!militaryUnits.empty())
	{
		cout << "MILITARY: [ ";
		for (shared_ptr<Tile> ptrTile : militaryUnits)
		{
			ptrTile->render();
			cout << " ";
		}
		cout << "] " << '\n';
	}

	if (tokenCounter.getTokenCount() > 0)
	{
		cout << "TOKENS: ";
		tokenCounter.render();
	}
}