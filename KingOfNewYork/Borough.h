#pragma once

#include <string>
#include <vector>
#include <memory>

#include "GameObject.h"
#include "Player.h"
#include "Deck.h"
#include "Tile.h"
#include "TokenCounter.h"

using std::string;
using std::vector;
using std::shared_ptr;

class Tile;
class Player;

class Borough : public GameObject
{
private:
	string name;
	vector<Player*> players;
	int player_limit;
	vector<Deck<Tile>> tileDecks;
	vector<shared_ptr<Tile>> militaryUnits;
	TokenCounter tokenCounter;

public:
	Borough();
	Borough(string name);
	~Borough();

	void setName(string name);
	string getName();
	void setPlayerLimit(int limit);
	int getPlayerLimit();
	vector<Player*>& getPlayers();
	bool addPlayer(Player* player);
	void removePlayer(Player* player);
	bool isFull();
	vector<Deck<Tile>>& getTileDecks();
	void setTileDecks(vector<Deck<Tile>> decks);
	vector<shared_ptr<Tile>>& getMilitaryUnits();
	TokenCounter& getTokenCounter();
	void render() const override;
};

