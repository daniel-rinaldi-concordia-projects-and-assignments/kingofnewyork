#include "Tile.h"

#include <iostream>

using std::cout;

Tile::Tile()
{
	player_point_type = EPlayerPointType::HEALTH;
}

Tile::Tile(ETileType type, ETileFace face, int durability)
{
	this->type = type;
	this->durability = durability;
	this->face = face;

	if (this->type == ETileType::BUILDING)
	{
		if (this->face == ETileFace::HOSPITAL)
			player_point_type = EPlayerPointType::HEALTH;
		else if (this->face == ETileFace::POWER_PLANT)
			player_point_type = EPlayerPointType::ENERGY;
		else if (this->face == ETileFace::OFFICE)
			player_point_type = EPlayerPointType::VICTORY;
		else
			player_point_type = EPlayerPointType::HEALTH;

		points = durability;
	}
	else if (this->type == ETileType::MILITARY)
	{
		if (this->face == ETileFace::INFANTRY)
			player_point_type = EPlayerPointType::HEALTH;
		else if (this->face == ETileFace::JET)
			player_point_type = EPlayerPointType::ENERGY;
		else if (this->face == ETileFace::TANK)
			player_point_type = EPlayerPointType::VICTORY;
		else
			player_point_type = EPlayerPointType::HEALTH;

		points = durability - 1;
	}
}

Tile::~Tile()
{
}

std::string Tile::tileTypeToString(ETileType type)
{
	if (type == ETileType::BUILDING)
		return "BUILDING";
	else if (type == ETileType::MILITARY)
		return "MILITARY";
	else
		return "";
}

std::string Tile::tileFaceToString(ETileFace face)
{
	if (face == ETileFace::HOSPITAL)
		return "HOSPITAL";
	else if (face == ETileFace::INFANTRY)
		return "INFANTRY";
	else if (face == ETileFace::JET)
		return "JET";
	else if (face == ETileFace::OFFICE)
		return "OFFICE";
	else if (face == ETileFace::POWER_PLANT)
		return "POWER_PLANT";
	else if (face == ETileFace::TANK)
		return "TANK";
	else
		return "";
}

Tile::ETileType Tile::getType()
{
	return type;
}

Tile::ETileFace Tile::getFace()
{
	return face;
}

EPlayerPointType Tile::getPointType()
{
	return player_point_type;
}

int Tile::getPoints()
{
	return points;
}

int Tile::getDurability()
{
	return durability;
}

void Tile::flip()
{
	if (type == ETileType::BUILDING)
	{
		if (durability == 1)
		{
			face = ETileFace::INFANTRY;
			player_point_type = EPlayerPointType::HEALTH;
		}
		else if (durability == 2)
		{
			face = ETileFace::JET;
			player_point_type = EPlayerPointType::ENERGY;
		}
		else if (durability == 3)
		{
			face = ETileFace::TANK;
			player_point_type = EPlayerPointType::VICTORY;
		}
		else
			face = ETileFace::INFANTRY;

		durability++;
		type = ETileType::MILITARY;
	}
}

void Tile::render() const
{
	std::cout << "(";
	std::cout << Tile::tileTypeToString(type) << ": " << Tile::tileFaceToString(face) << " | ";
	std::cout << Player::playerPointTypeToString(player_point_type) << ": " << points << " | ";
	std::cout << "DURABILITY: " << durability;
	std::cout << ")";
}

string Tile::toString()
{
	string str = "";
	str += "{type:" + Tile::tileTypeToString(type) + ", face:" + Tile::tileFaceToString(face) +
		", points:" + std::to_string(points) + ", player_point_type:" + Player::playerPointTypeToString(player_point_type) +
		", durability:" + std::to_string(durability) + "}";
	return str;
}