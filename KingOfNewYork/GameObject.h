#pragma once

class GameObject
{
public:
	virtual void render() const = 0;
};

