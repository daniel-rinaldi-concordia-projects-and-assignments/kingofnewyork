#pragma once

#include <vector>
#include <map>

#include "Player.h"
#include "Deck.h"
#include "Tile.h"
#include "Borough.h"
#include "Card.h"

class Player;
class Borough;
class Card;

class PlayerStrategy
{
protected:
	Player* player;

public:
	PlayerStrategy(Player* player);
	virtual ~PlayerStrategy();

	virtual int chooseCharacter(std::vector<std::string> characters) = 0;
	virtual int chooseStartingBorough(std::vector<std::string> boroughLabels) = 0;
	virtual void rollDice(std::vector<int>& initialRoll) = 0;
	virtual void resolveDice(std::map<Dice::EDiceSymbols, int> diceResults) = 0;
	virtual bool flee() = 0;
	virtual int destroy(std::vector<Deck<Tile>*> buildingTargets, std::vector<std::shared_ptr<Tile>> militaryTargets) = 0;
	virtual void move(std::vector<Borough*> movementOptions, bool mandatoryMovement) = 0;
	virtual void buyCards(std::vector<std::shared_ptr<Card>>& shop) = 0;
};

